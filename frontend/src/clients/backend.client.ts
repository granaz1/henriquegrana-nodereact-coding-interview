import axios, { AxiosRequestConfig } from "axios";
import { IUserProps } from "../dtos/user.dto";

interface getAllUsersProps {
  searchTerm?: string,
  limit?: number
}

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getAllUsers({
    searchTerm,
    limit
  }: getAllUsersProps): Promise<{ data: IUserProps[] }> {
    const params: AxiosRequestConfig = {
      params: {
        searchTerm,
        limit,
      }
    }
    return (await axios.get(`${this.baseUrl}/people/all`, params)).data;
  }
}
