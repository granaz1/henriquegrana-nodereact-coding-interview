import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";
import { FormLabel, Input } from "@material-ui/core";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [inputValue, setInputValue] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!users.length && !loading) {
      setLoading(true);

      backendClient.getAllUsers({}).then((result) => {
        setUsers(result.data);
        setLoading(false);
      })
    }
  }, [users, loading]);

  useEffect(() => {
    if (inputValue !== '') {
      setLoading(true);

      backendClient.getAllUsers({
        searchTerm: inputValue
      }).then((result) => {
        setUsers(result.data);
        setLoading(false);
      })
    }
  }, [ inputValue ])

  return (
    <div style={{ paddingTop: "30px" }}>
      <div>
        <FormLabel>Type here to search</FormLabel>
        <Input onChange={(e) => setInputValue(e.target.value)} value={inputValue} />
      </div>

      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user?.id} {...user} />;
                })
              : null}
          </div>
        )}
      </div>
    </div>
  );
};
